#!/bin/bash
# main.sh script

tpassword=m
tlogin=Alex

#indicates all the commands the user can use
ft_help(){
    echo -e "help : will indicate which command you can use\nls : show a list of visible and invisible files\nrm : deletes a file\nrmd or rmdir : deletes a directory\nabout : describes the program\nversion or --v or vers : shows the version of the prompt\nage : asks for your age and will tell you if you're minor or an adult\nquit : exit the program\nprofile : shows your information, First Name, Last name, Age and email\ncd : Allows you to switch directory, type cd .. to move to the previous directory\npassw : will let you change your password after a confirmation\npwd : tells you in which directory you are\nhour : tells you what time it is\nhttpget : allows you to download the source code of an HTML page and to save it into a specific file, the prompt will ask the name of the file\nsmtp : make you send an email with an adress a subject and a body\nopen : opens a file with vim even if it doesn't exist\nrps : starts a 2 player Rock Paper Scissors game\n* : shows an unknown command"
}

#allows the user to change its password
ft_password(){
    echo -e "To change your password please enter your old password :\n"
    read -s passwordtest
    if [ "$passwordtest" != "$password" ]; then
        echo "permission denied"
        exit
    fi
    echo -e "Authorised, please enter your new password"
    read -s password
    tpassword=password
}

#allows the user to change its password
ft_password(){
    echo -e "To change your password please enter your old password :\n"
    read -s passwordtest
    if [ "$passwordtest" != "$password" ]; then
        echo "permission denied"
        exit
    fi
    echo -e "Authorised, please enter your new password"
    read -s password
    tpassword=password
}

#asks for your age and tells you if you're minor major or not even born
ft_age(){
    echo -e "\nHow old are you ?"
    read age
    if [ $age -lt 0 ]; then
        echo "you aren't even born yet"
    elif [ $age -lt 18 ]; then
        echo "you're a minor"
    else [ $age -gt 18 ];
        echo "congrats you're an adult"
    fi
}

#check first if the file exist and then deletes it
ft_deletefile(){
    if [ -f "$argv0" ]; then
        echo "file deleted"
        rm $argv0
    else
        echo "$argv0 doesn't exist"
    fi
}

#check if a directory exist and then deletes it
ft_deletefolder(){
    if [ "$argv0" == "*" ]; then
        echo "how dare you ? get out"
        exit
    fi    
    if [ -d "$argv0" ]; then
        echo "directory deleted"
        rm -rf $argv0
    else
        echo "$argv0 doesn't exist"
    fi
}

#decribes the program
ft_about(){
    echo -e "prompt is a program that allows its user to do many things such as : \nlist files, delete files, indicate where you are, send mail, tell you what time is it and many more thing\ntype help to find out what else you can do with your magic prompt\n" 
}

#gives your informations (in case you forget them)
ft_profile(){
    echo "You're Alex MOULINNEUF, 23 years old and your mail adress is moulinneufalex@gmail.com"
}

#download the source code of an html page
ft_downloadtext(){
    echo -e "Enter the file name in which you want to save the source code of the web page: \n"
    read nomfichier
    curl -o ${nomfichier}.html ${argv0}
}

#sends and email, will ask you for every necessary informations
ft_mail(){
    echo "enter the mail recipient"
    read mail
    echo "what is the subject of your mail"
    read subject
    echo "What is your message"
    read message
    mail -s $subject $mail <<< $message
    echo "Votre message a bien été envyoyé"
}


#allows 2 player to play Rock Paper Scissors
ft_rps(){
    while [ "$gameon" != 0 ]
    do
        echo "Enter a name for player 1"
        read pone
        echo "Enter a name for player 2"
        read ptwo
        clear
        gameon=1
        turn=3
        ponepoints=0
        ptwopoints=0
        while [ "$turn" != 0 ]
        do
            echo "$pone it's your turn Rock Paper or Scissors"
            read -s ponemove
            clear
            echo "$ptwo it's your turn Rock Paper or Scissors"
            read -s ptwomove
            clear
            echo "Rock paper scissors !"
            echo "$pone $ponemove" 
            echo "$ptwo $ptwomove"
            if [ "$ponemove" == "Rock" ] && [ "$ptwomove" == "Scissors" ]; then
                echo "Rock beats Scissors"
                echo "$pone wins"
                ponepoints=$(($ponepoints+1))
                turn=$(($turn-1))
                echo "Score : $pone $ponepoints $ptwo $ptwopoints"
            elif [ "$ponemove" == "Scissors" ] && [ "$ptwomove" == "Paper" ]; then
                echo "Scissors beats Paper"
                echo "$pone wins"
                ponepoints=$(($ponepoints+1))
                turn=$(($turn-1))
                echo "Score : $pone $ponepoints $ptwo $ptwopoints"
            elif [ "$ponemove" == "Paper" ] && [ "$ptwomove" == "Rock" ]; then
                echo "Paper beats Rock"
                echo "$pone wins"
                ponepoints=$(($ponepoints+1))
                turn=$(($turn-1))
                echo "Score : $pone $ponepoints $ptwo $ptwopoints"
            elif [ "$ptwomove" == "Scissors" ] && [ "$ponemove" == "Paper" ]; then
                echo "Scissors beats Paper"
                echo "$ptwo wins"
                ptwopoints=$(($ptwopoints+1))
                turn=$(($turn-1))
                echo "Score : $pone $ponepoints $ptwo $ptwopoints"
            elif [ "$ptwomove" == "Paper" ] && [ "$ponemove" == "Rock" ]; then
                echo "Paper beats Rock"
                echo "$ptwo wins"
                ptwopoints=$(($ptwopoints+1))
                turn=$(($turn-1))
                echo "Score : $pone $ponepoints $ptwo $ptwopoints"
            elif [ "$ptwomove" == "Rock" ] && [ "$ponemove" == "Scissors" ]; then
                echo "Rock beats Scissors"
                echo "$ptwo wins"
                ptwopoints=$(($ptwopoints+1))
                turn=$(($turn-1))
                echo "Score : $pone $ponepoints $ptwo $ptwopoints"
            elif [ "$ptwomove" == "$ponemove" ]; then
                echo "draw"
            elif [ "$ptwomove" == "SuperKitty" ]; then
                echo "$ptwo wins"
                ptwopoints=$(($ptwopoints+1))
                turn=$(($turn-1))
                echo "Score : $pone $ponepoints $ptwo $ptwopoints"
            elif [ "$ponemove" == "SuperKitty" ]; then
                echo "$pone wins"
                ponepoints=$(($ponepoints+1))
                turn=$(($turn-1))
                echo "Score : $pone $ponepoints $ptwo $ptwopoints"
            fi
        done
        if [ "$ponepoints" -gt "$ptwopoints" ]; then
            echo "$pone won the game"
        else 
            echo "$ptwo won the game"
        fi
        echo "One more game ? Y to continue N to stop"
        read answer
        if [ "$answer" == "N" ]; then
            gameon=$(($gameon-1))
        fi
    done
}

#the magic-prompt, a basically mini terminal
main(){
    echo -n "Login : "
    read login
    echo -n "Password : " 
    read -s password
    if [ "$login" != "$tlogin" ] || [ "$password" != "$tpassword" ]; then
        echo "Login failed"
        exit
    fi
    echo -e "\n\nWelcome ${login}"
    prompt=1
    while [ "$prompt" != 0 ]
    do
        echo "What do you want to do ? (type help for command)"
        read cmd argv0
        case $cmd in  
            "help" ) ft_help;;
            "ls" ) ls -a;;
            "about" ) ft_about;;
            "cd" ) cd $argv0;;
            "pwd" ) pwd;;
            "passw" ) ft_password;;
            "hour" ) date +%R;;
            "quit" ) prompt=$(($prompt-1));;
            "version" | "vers" | "--v" ) echo "Prompt version 0.0.2";;
            "httpget" ) ft_downloadtext $argv0;;
            "rm" ) ft_deletefile;;
            "rmd" | "rmdir" ) ft_deletefolder;;
            "open" ) vim $argv0;;
            "age" ) ft_age;;
            "profile" ) ft_profile;;
            "rps" ) ft_rps;;
            "smtp" ) ft_mail;;
            * ) echo "Unknown command";;
        esac
    done   
}

main