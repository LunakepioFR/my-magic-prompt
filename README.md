
# my-magic-prompt

Documentation created on 23 September 2021 for version 4.0.4 of my-magic-prompt

Copyright © 2021 Moulinneuf ALEX

Copying and distribution of this file, with or without modification, are permitted in any medium without royalty provided the copyright notice and this notice are preserved. These files are offered as-is, without any warranty.

## Introduction 

This programme is a micro shell.

It works in default mode of operation, it list files, delete files or directory, asks for your age and tells you if you're minor or major, gives your informations, allows you to change your password, to switch directory, to indicate in which repertory you are, to tell you what time is it, to download the source code of an HTML web page, to play a Rock Paper Scissors game with another player and to open a file with vim even if it doesn't exist.

## How to use my-magic-prompt

To start the program, type ./main.sh in the shell in the "my-magic-prompt" directory, you'll have to login with your username and password 

## Commands

'help'
will tell you everything you can do with my-magic-prompt

'about'
This command will give you a short description of what the program can do

'quit'
Exit the program

'ls'
list files and show visible and hidden files

'rm'
check if a file exist and deletes a file

'rmd' or 'rmdir'
check if a directory exist and deletes a directory

'passw'
will let you change your password

'cd'
allows you to move between directory

'pwd'
indicates in which directory you are

'httpget'
download the source code of an html web page and save it into a file in which you specify the name

'smtp'
sends an email with an recipient, a subject and a message

'version' or 'vers' or '--v'
will tell you which version of my-magic-prompt you're yousing

'age'
asks for your age and will tell you if you're minor, major, or not even born

'hour'
indicates the current hour

'profile'
gives you your information with first name, last name and mail adress

'open'
opens a file with vim editor even if the file doesn't exist
